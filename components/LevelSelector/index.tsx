/**
 * Select level based on inverse prymaid selector
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import {
  View,
  GestureResponderEvent,
  ViewStyle,
  TouchableWithoutFeedback,
} from 'react-native'
import React from 'react'
import tw from '../../lib/tailwind'
import { COLORS } from '../../config/theme'

export type LevelSelectorProps = {
  max: number
  value: number
  onPress?: (value: number, event: GestureResponderEvent) => void
  style?: ViewStyle
}

const LevelSelector: React.FC<LevelSelectorProps> = ({
  max,
  onPress,
  value,
}) => {
  return (
    <View style={tw`items-center`}>
      {Array.from(Array(max).keys())
        .reverse()
        .map((level, index) => {
          return (
            <TouchableWithoutFeedback
              key={level}
              onPress={event => onPress && onPress(level, event)}
              disabled={value === level}
            >
              <View
                style={[
                  tw.style(`h-0 my-2 rounded-full`),
                  {
                    width: `${100 - (100 / max) * index}%`,
                    borderBottomColor:
                      value >= level ? '#fff' : COLORS.primaryShade,
                    borderBottomWidth: 40,
                    borderRightColor: 'transparent',
                    borderRightWidth: 25,
                    borderLeftColor: 'transparent',
                    borderLeftWidth: 25,
                    transform: [
                      {
                        rotate: '180deg',
                      },
                    ],
                  },
                ]}
              />
            </TouchableWithoutFeedback>
          )
        })}
    </View>
  )
}

export default LevelSelector
