/**
 * Slider
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import React from 'react'
import { View } from 'react-native'
import { Slider as RNSlider } from '@miblanchard/react-native-slider'
import { SliderProps as RNSliderProps } from '@miblanchard/react-native-slider/lib/types'

import { COLORS } from '../../config/theme'
import tw from '../../lib/tailwind'

export type SliderProps = Omit<
  RNSliderProps,
  'animationType' | 'onValueChange'
> & {
  onValueChange: (value: number) => void
}

const Slider: React.FC<SliderProps> = ({ onValueChange, ...rest }) => {
  return (
    <View>
      <RNSlider
        animationType="timing"
        trackStyle={{
          height: 18,
          borderRadius: 50,
        }}
        minimumTrackTintColor={COLORS.secondary}
        maximumTrackTintColor="#fff"
        onValueChange={value =>
          onValueChange(typeof value === 'number' ? value : value[0])
        }
        renderThumbComponent={() => (
          <View
            style={[
              tw`rounded-full justify-center items-center`,
              {
                height: 28,
                width: 28,
                backgroundColor: '#c8f7fc',

                // Android
                elevation: 5,

                // IOS
                shadowOffset: { width: -2, height: 4 },
                shadowOpacity: 0.2,
                shadowRadius: 3,
              },
            ]}
          >
            <View
              style={[
                tw`bg-secondary rounded-full`,
                {
                  height: 18,
                  width: 18,
                },
              ]}
            />
          </View>
        )}
        {...rest}
      />
    </View>
  )
}

export default Slider
