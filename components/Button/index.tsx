/**
 * Custom button
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import { Text, TouchableHighlight, TouchableHighlightProps } from 'react-native'
import React from 'react'
import tw from '../../lib/tailwind'
import { COLORS } from '../../config/theme'

export type ButtonProps = TouchableHighlightProps & {
  title?: string
}

const Button: React.FC<ButtonProps> = ({
  onPress,
  style,
  children,
  title,
  underlayColor,
  ...rest
}) => {
  return (
    <TouchableHighlight
      onPress={onPress}
      underlayColor={underlayColor || COLORS.secondary}
      style={[tw`rounded-full bg-white p-3`, style]}
      {...rest}
    >
      <Text style={tw`font-bold text-center`}>{title || children}</Text>
    </TouchableHighlight>
  )
}

export default Button
