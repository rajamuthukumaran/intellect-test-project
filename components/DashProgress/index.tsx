/**
 * Screen progress bar
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import { View } from 'react-native'
import React from 'react'
import tw from '../../lib/tailwind'

export type DashProgressProps = {
  size: number
  progress: number
}

const DashProgress: React.FC<DashProgressProps> = ({ size, progress }) => {
  return (
    <View style={tw.style('justify-between flex-row -mx-2')}>
      {[...Array(size || 1)]?.map((e, index) => {
        return (
          <View
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            style={[
              tw.style(
                'flex-1 mx-2 rounded',
                progress < index ? 'bg-primary-shade' : 'bg-white',
              ),
              {
                height: 5,
              },
            ]}
          />
        )
      })}
    </View>
  )
}

export default DashProgress
