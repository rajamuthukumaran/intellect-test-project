/**
 * Empty touchableHighlight pre designe wrapper for icon button
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import { TouchableHighlight, TouchableHighlightProps } from 'react-native'
import React from 'react'
import tw from '../../lib/tailwind'
import { COLORS } from '../../config/theme'

export type IconButtonProps = TouchableHighlightProps

const IconButton: React.FC<IconButtonProps> = ({
  children,
  style,
  underlayColor,
  ...rest
}) => {
  return (
    <TouchableHighlight
      {...rest}
      style={[tw`rounded-full`, style]}
      underlayColor={underlayColor || COLORS.secondary}
    >
      {children}
    </TouchableHighlight>
  )
}

export default IconButton
