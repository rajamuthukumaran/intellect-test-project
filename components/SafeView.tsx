/**
 * Adds padding at top based on status bar of device.
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import React from 'react'
import {
  SafeAreaView,
  View,
  ViewProps,
  Platform,
  StatusBar,
  StyleSheetProperties,
} from 'react-native'
import tw from '../lib/tailwind'

export type SafeViewProps = {
  style?: StyleSheetProperties
} & ViewProps

const SafeView: React.FC<SafeViewProps> = ({ children, style, ...rest }) => {
  return (
    <SafeAreaView
      style={[
        {
          paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
        },
        tw.style('flex-1 bg-primary'),
      ]}
    >
      <View style={[style, tw.style('flex-1')]} {...rest}>
        {children}
      </View>
    </SafeAreaView>
  )
}

export default SafeView
