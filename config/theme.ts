export const COLORS = {
  primary: '#325d6e',
  secondary: '#6edbe6',
  primaryShade: '#819da7',
}
