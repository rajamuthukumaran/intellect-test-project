import React from 'react'
import { StatusBar } from 'expo-status-bar'
import { View } from 'react-native'
import SafeView from './components/SafeView'
import RescureSession from './screens/RescueSession'
import tw from './lib/tailwind'

export default function App() {
  return (
    <SafeView>
      <View style={tw`flex-1 items-center justify-center h-full`}>
        <RescureSession />
        {/* eslint-disable-next-line react/style-prop-object */}
        <StatusBar style="auto" />
      </View>
    </SafeView>
  )
}
