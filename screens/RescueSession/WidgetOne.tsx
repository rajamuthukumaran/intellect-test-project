/**
 * Anger level selector widget based on slider
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import { View, Text } from 'react-native'
import React, { useState } from 'react'
import { AnimatedCircularProgress } from 'react-native-circular-progress'

import { WidgetProps } from './WidgetTwo'
import tw from '../../lib/tailwind'
import { COLORS } from '../../config/theme'
import Slider from '../../components/Slider'

const WidgetOne: React.FC<WidgetProps> = ({ screenNo, currentScreen }) => {
  const [level, setLevel] = useState(0)

  // Not render if current screen is not active
  if (screenNo !== currentScreen) return null

  return (
    <View style={tw`justify-center`}>
      <View style={tw`items-center`}>
        {/* Dashed border */}
        <View
          style={[
            tw`rounded-full justify-center items-center mb-16`,
            {
              height: 300,
              width: 300,
              borderWidth: 3,
              borderStyle: 'dashed',
              borderColor: COLORS.primaryShade,
            },
          ]}
        >
          {/* Animated circular progress */}
          <AnimatedCircularProgress
            size={250}
            width={8}
            fill={level * 10}
            tintColor="#fff"
            padding={0}
            rotation={0}
            lineCap="round"
          >
            {() => (
              // Anger level value
              <View
                style={[tw`h-full w-full p-10`, { backgroundColor: '#456d7c' }]}
              >
                <View
                  style={[
                    tw`h-full w-full rounded-full justify-center items-center`,
                    { backgroundColor: COLORS.secondary },
                  ]}
                >
                  <Text style={tw`text-4xl font-bold text-white`}>{level}</Text>
                </View>
              </View>
            )}
          </AnimatedCircularProgress>
        </View>
      </View>

      <Slider
        minimumValue={0}
        maximumValue={10}
        value={level}
        onValueChange={setLevel}
        step={1}
      />
    </View>
  )
}

export default WidgetOne
