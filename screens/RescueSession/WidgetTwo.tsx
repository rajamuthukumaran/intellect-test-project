/**
 * Anger level selector widget based on inverse prymaid selector
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import { View, Text } from 'react-native'
import React, { useMemo, useState } from 'react'
import LevelSelector from '../../components/LevelSelector'
import tw from '../../lib/tailwind'

export type WidgetProps = {
  currentScreen: number
  screenNo: number
}

const WidgetTwo: React.FC<WidgetProps> = ({ screenNo, currentScreen }) => {
  const [level, setLevel] = useState(0)

  // Get level intensity based on selected level from prymaid
  const moodLevel = useMemo(() => {
    switch (level) {
      case 0:
        return 'Very Low'
      case 1:
        return 'Low'
      case 2:
        return 'Medium'
      case 3:
        return 'High'
      case 4:
        return 'Very High'
      default:
        return 'Low'
    }
  }, [level])

  // Not render if current screen is not active
  if (screenNo !== currentScreen) return null

  return (
    <View style={tw`justify-center px-8`}>
      <Text style={tw`text-center text-white text-lg font-semibold mb-5`}>
        {moodLevel}
      </Text>
      <View>
        <LevelSelector max={5} value={level} onPress={setLevel} />
      </View>
    </View>
  )
}

export default WidgetTwo
