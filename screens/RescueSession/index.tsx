/**
 * Rescue session for Anger and frustration screen
 *
 * @author [Rajamuthukumaran D](https://gitlab.com/rajamuthukumaran)
 */

import React, { useState } from 'react'
import { View, Text } from 'react-native'
import { AntDesign } from '@expo/vector-icons'
import DashProgress from '../../components/DashProgress'
import IconButton from '../../components/IconButton'
import tw from '../../lib/tailwind'
import WidgetTwo from './WidgetTwo'
import Button from '../../components/Button'
import WidgetOne from './WidgetOne'

const RescureSession: React.FC = () => {
  const TOTAL_SCREENS = 5
  const [screen, setScreen] = useState<number>(0)

  return (
    <View
      style={tw`bg-primary h-full w-full pt-5 pb-10 px-5 mt-5 justify-between`}
    >
      <View>
        <DashProgress size={TOTAL_SCREENS} progress={screen} />

        <View style={tw`py-3 flex-row justify-between items-center`}>
          <Text style={tw`text-white font-semibold`}>
            RESCUE SESSION: ANGER & FRUSTRATION
          </Text>
          <IconButton disabled>
            <AntDesign name="close" size={24} color="white" style={tw`p-1`} />
          </IconButton>
        </View>

        <View style={tw`mt-3`}>
          <Text style={tw`text-white font-bold text-2xl`}>
            Pick the level your anger & frustation right now
          </Text>
        </View>

        <View style={tw`mt-12`}>
          <WidgetOne screenNo={0} currentScreen={screen} />
          <WidgetTwo screenNo={1} currentScreen={screen} />
        </View>
      </View>

      <View>
        {!!screen && (
          <Button
            style={tw`mb-3 bg-primary-shade`}
            onPress={() => setScreen(prev => prev - 1)}
            title="Back"
          />
        )}
        {screen < TOTAL_SCREENS - 1 && (
          <Button onPress={() => setScreen(prev => prev + 1)} title="Next" />
        )}
      </View>
    </View>
  )
}

export default RescureSession
